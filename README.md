Non-project specific CI resources. For example a generic Docker image to speed up CI by not having to install all requirements test/build every time.
